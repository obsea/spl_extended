from numpy import log10, power, fft, nan
import plots
import math

thirdOctaveFc = [25,31.5,40,50,63,80,100,125,160,200,250,315,400,500,630,800,1000,1250,
                 1600,2000,2500,3150,4000,5000,6300,8000,10000,12500,16000,20000]

def thirdLowHighFreq():
    """
    This function computes low and high frequencies for each third octave band
    :return: A list containing each central frequency and both low and high frequencies
    """
    thirdOctave = []
    for fc in thirdOctaveFc:
        fl = fc/power(2,1/6)
        fh = 0.23*fc + fl
        thirdOctave.append([fl,fc,fh])
    thirdOctave.append([thirdOctave[-1][-1],35000,48000])
    return thirdOctave



def avoidZerosNan(data):
    """
    This aux function replace zeros of data with very small data
    :param data: data that we want to avoid zeros
    :return: same data vector with zeros replaced
    """
    for i in range(len(data)) :
        if data[i] == 0 or math.isnan(data[i]):
            data[i] = 0.00000000001 # avoid 0
    return data


def getPressure(data, sensitivitydB, gaindB, k):
    """
    This function returns the pressure signal (uPa) computed from counts signal
    :param data: raw data
    :param sensitivitydB: ref 1V/1microPa
    :param gaindB: Received gain
    :param k: ADC quantification step
    :return: Pressure signal (uPa)
    """

    sensitivity = power(10, sensitivitydB / 20)
    gain = power(10, gaindB / 20)
    inputData = avoidZerosNan(data)

    return inputData / (sensitivity * gain * k)

def getPowerSpectrum(pressure):
    """
    This funcition returns the fft of a pressure vector computed over the positive frequency axis
    :param pressure: Pressure signal
    :return: Energy spectrum over the positive frequency axis
    """
    pressureFreq = abs(fft.rfft(pressure)) / len(pressure)
    return 2 * power(pressureFreq, 2)


def timeBasedSPL(data, sensitivitydB, gaindB, k):
    """
    This function returns the average SPL of the received data over the whole acquisition time
    :param data: raw data
    :param sensitivitydB: ref 1V/1microPa
    :param gaindB: Received gain
    :param k: ADC quantification step
    :return: Average SPL
    """

    #dB to lineal units
    pressure = getPressure(data, sensitivitydB, gaindB, k)
    #Energy
    pow = power(pressure, 2)
    #Return energy integration over signal duration and return result on dB
    return 10 * log10(sum(pow) / len(pow))

def freqBasedSPL(data, sensitivitydB, gaindB, k):
    """
    This function returns the average SPL of the received data computed over frequency spectrum
    :param data: raw data
    :param sensitivitydB: ref 1V/uPa
    :param gaindB: Received gain
    :param k: ADC quantification step
    :return: Average SPL
    """

    #dB to lineal units
    pressure = getPressure(data, sensitivitydB, gaindB, k)
    #frequency energy spectrum
    powerFreq = getPowerSpectrum(pressure)
    return 10 * log10(sum(powerFreq))

def bandPower(data, indexLow, indexHigh):
    """
    This function returns the power between two indexes of a vector
    :param data: Complete data to compute power
    :param indexLow: index of first vector position to compute
    :param indexHigh: index of last vector position to compute
    :return: computed power of the specified band
    """
    pow = 0
    for i in range(indexLow, indexHigh):
        pow = pow + data[i]
    return pow

def freqBandsSPL(data, sensitivitydB, gaindB, k, samplinfFreq, plot):
    """
    This function returns a vector containing the low, central and high frequencies of each third octave band
    and the power of each third octave band on lineal units
    :param data:  Vector with power spectrum
    :param sensitivitydB: ref 1V/uPa
    :param gaindB: Received gain
    :param k: ADC quantification step
    :param samplinfFreq: Sampling frequencie used to capture data
    :return: vector of frequencies and power of each third octave band
    """
    #dB to lineal units
    pressure = getPressure(data, sensitivitydB, gaindB, k)
    #frequency energy spectrum
    powerFreq = getPowerSpectrum(pressure)
    freqAxis = fft.rfftfreq(len(pressure),1/samplinfFreq)
    if plot:
        plots.plotxy(freqAxis,10 * log10(powerFreq),  "Freq (HZ)", "SPL (dB)")
    thirdOctaves = thirdLowHighFreq()
    powerThird = []
    step = freqAxis[-1] - freqAxis[-2]

    for i in range(len(thirdOctaves)):
        low = int(thirdOctaves[i][0]/step)
        high = int(thirdOctaves[i][2]/step)

        #print("Freq Low: ",freqAxis[low])
        #print("Index Low:",low)
        #print("Freq: ",thirdOctaves[i][1])
        #print("Freq High: ",freqAxis[high])
        #print("Index High: ", high)
        pow = bandPower(powerFreq, low, high)
        #print("Power: ",10 * log10(pow))
        powerThird.append([thirdOctaves[i],pow])
    return powerThird




