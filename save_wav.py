#!/usr/bin/pytho3
import os
import time
import wave
from numpy import int16
import struct




def save_to_wavfile(data, samplingFreq, path):
    # Check if full path exists (Year Month Day Hour Minute)
    folder = path + '/' + time.strftime('%Y/%m/%d/%H/%M')
    if not os.path.exists(folder): 
        os.makedirs(folder)        
    
    timestamp = time.time()
    filename = folder + '/' + str(timestamp) +'.wav'
    print("[WAV]: saving wav file", filename)
            
    wavfile = wave.open(filename, 'w') 
    audioData = int16(data)
    wavfile.setparams((1,2, samplingFreq, 0, 'NONE', 'not compressed'))
    values = struct.pack('='+str(len(audioData))+'h', *audioData)


#    value_str = ''.join(values)
    
    
    wavfile.writeframes(values)
    wavfile.close()