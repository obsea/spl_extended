#!/usr/env python3
# -----------------------------------------------------------------------------#
# This program provides an interface to communicate and gather data from a
# Naxys Ethernet Hydrohone 02345
# -----------------------------------------------------------------------------#

from socket import socket, AF_INET, SOCK_DGRAM
from numpy import ndarray, mean, append, sum, log10, arange, power, conj, zeros, full, nan
from numpy.fft import fft
from struct import unpack
from plots import plotxy, plot2xy
import sys
from matplotlib import pyplot

# Constants
BLU = "\x1B[34m"
RST = "\033[0m"
GRN = "\x1B[32m"
BLU = "\x1B[34m"
YEL = "\x1B[33m"
RED = "\x1B[31m"
MAG = "\x1B[35m"
CYN = "\x1B[36m"
WHT = "\x1B[37m"
NRM = "\x1B[0m"
RST = "\033[0m"


# Naxys Hydrophone Class #
class NaxysHydrophone():
    def __init__(self, port):
        # Private Variables #
        self.__openedPort = False
        self.__name = "NaxysHydrophone:"
        self.sensitivitydB = -192  # dB re 1V/1uPa @ 10kHz
        self.__packetSize = 1026
        self.__samplesPerPacket = 512
        self.__bits = 16
        self.__emptyPacket = full((self.__samplesPerPacket,), nan)

        self.voltageConversion = (power(2, (self.__bits - 1)) - 1) / 2.5
        self.voltageConversiondB = 20 * log10(self.voltageConversion)

        # Initialize internal variables
        self.UDPport = port

        self.gain = -1  # last Gain value received from the Hydrophone
        self.offset = 0
        self.samplingFreq = -1
        self.packetCount = -1  # last UDP stream count received

        self.msg("Initializing NaxysHydrophone class")

        # Get first packet to set SamplingRate and Gain #
        self.open()
        self.receivePacket()
        self.close()
        self.info()

    def msg(self, *args):
        sys.stdout.write(BLU)
        print(self.__name, *args)
        sys.stdout.write(RST)

    def warnmsg(self, *args):
        sys.stdout.write(YEL)
        print("WARNING", self.__name, *args)
        sys.stdout.write(RST)

    # Opens an UDP socket at @port to receive data from the hydrophone
    def open(self):
        port = self.UDPport
        # self.msg("Opening socket UDP at port", port)
        self.UDPport = port
        self.socket = socket(AF_INET, SOCK_DGRAM)  # open socket
        self.socket.bind(('', port))  # bind socket to port
        self.__openedPort = True

    def close(self):
        # self.msg("Closing UDP port....")
        self.socket.close()
        self.__openedPort = False

    def info(self):
        self.msg("Gain", self.gain, "dB")
        self.msg("Sampling Frequency", self.samplingFreq / 1000, "kHz")
        self.msg("Sensitivity", self.sensitivitydB, "dB re 1V/1uPa")

    # Receives an UDP Stream, processes it and returns an ndarray
    # of. The offset is eliminated from the stream
    # if the raw flag is set, the offset is not suppressed
    def receivePacket(self, raw=False):
        rawdata = self.socket.recv(self.__packetSize)
        self.packetCount = rawdata[0]
        self._getFreqGain(rawdata[1])

        # Convert the data from bytes to 16-bit int (little-endian)
        array = ndarray((self.__samplesPerPacket,), dtype='<i2', buffer=rawdata[2:])

        # array = ndarray((self.__samplesPerPacket,), dtype='>h', buffer=rawdata[2:])

        # delete the offset
        if raw == False:
            self.offset = mean(array)
            array = array - self.offset

        return array


    # accumulates X seconds of data
    def accummulateData(self, seconds, raw=False):
        # Calculate number of packets        
        if self.__openedPort == False:
            self.open()

        npackets = int(seconds * self.samplingFreq / self.__samplesPerPacket)
        self.msg("Acquiring during", seconds, "s (", npackets, "packets)")

        lostPackets = 0

        data = self.receivePacket(raw)
        lastPacketCount = self.packetCount  #
        totalPacketsCount = 1

        while totalPacketsCount < npackets:
            # dataRaw = append(dataRaw,hydrophone.receivePacket())
            newPacket = self.receivePacket(raw)


            totalPacketsCount += 1

            # Check if we have lost some packet        
            if ((lastPacketCount + 1) % 256) != self.packetCount:
                for i in range((lastPacketCount + 1) % 256, self.packetCount):
                    data = append(data, self.__emptyPacket)
                    lostPackets += 1
                    totalPacketsCount += 1
                self.warnmsg("Lost packets: ", arange((lastPacketCount + 1) % 256, self.packetCount))

            data = append(data, newPacket)
            lastPacketCount = self.packetCount

        if lostPackets > 0:
            self.warnmsg("lost packets:", lostPackets)

        self.close()
        return data

    # This function takes the second byte from the stream and updates
    # the gain and samplingfreqBits variables
    def _getFreqGain(self, b):
        bitString = bin(b)  # returns a string like "0b01010101"
        bits = bitString[2:]  # remove the leading chars "0b"
        freqBits = bits[0:4]  # first 4 bits in the string
        gainBits = bits[4:6]

        # Get Gain
        if gainBits == '11':
            self.gain = 0
        elif gainBits == '10':
            self.gain = 10
        elif gainBits == '01':
            self.gain = 20
        elif gainBits == '00':
            self.gain = 40

        # Get freqBitsuency
        if freqBits == '1110':
            self.samplingFreq = 768000
        elif freqBits == '1101':
            self.samplingFreq = 384000
        elif freqBits == '1100':
            self.samplingFreq = 192000
        elif freqBits == '1011':
            self.samplingFreq = 96000
        elif freqBits == '1010':
            self.samplingFreq = 88200
        elif freqBits == '1001':
            self.samplingFreq = 48000
        elif freqBits == '1000':
            self.samplingFreq = 44100
        elif freqBits == '0111':
            self.samplingFreq = 24000
        elif freqBits == '0110':
            self.samplingFreq = 12000
        elif freqBits == '0100':
            self.samplingFreq = 11025
        elif freqBits == '0011':
            self.samplingFreq = 8000
        elif freqBits == '0010':
            self.samplingFreq = 6000
        elif freqBits == '001':
            self.samplingFreq = 5512.5
        elif freqBits == '0000':
            self.samplingFreq = 2000