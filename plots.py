#!/usr/env python3

from matplotlib import pyplot
from wave import open
from numpy import int16



# plot an array 
def plot(array, xlabel1='', ylabel1=''):
    pyplot.figure(1)    
    pyplot.ylabel(ylabel1)
    pyplot.xlabel(xlabel1)
    pyplot.plot(array)
    pyplot.show()


def plotxy(xarray, yarray, xlabel="", ylabel=""):
    if len(xarray) > 0 and len(yarray) > 0 :
        pyplot.figure(1)        
        pyplot.plot(xarray, yarray)
        pyplot.ylabel(ylabel)
        pyplot.xlabel(xlabel)                        
        pyplot.show()             
            
def plotxylog(xarray, yarray, xlabel, ylabel):
    if len(xarray) > 0 and len(yarray) > 0 :
        pyplot.figure(1)  
        #  pyplot.yscale('log')
        pyplot.xscale('log')
        pyplot.plot(xarray, yarray)
        pyplot.ylabel(ylabel)
        pyplot.xlabel(xlabel)                        
        pyplot.show()             
            


def plots2(array1, xlabel1='', ylabel1='',  array2=[], xlabel2='', ylabel2=''):
    if len(array1) > 0 and len(array2) > 0 :
        pyplot.figure(1)
        pyplot.subplot(211)
        pyplot.plot(array1)
        pyplot.ylabel(ylabel1)
        pyplot.xlabel(xlabel1)
        pyplot.subplot(212)
        pyplot.plot(array2)
        pyplot.ylabel(ylabel2)
        pyplot.xlabel(xlabel2)
        pyplot.show()             
    else :
        return None
    
    
def plot2xy(xarray1, xlabel1, yarray1, ylabel1,xarray2, xlabel2,yarray2, ylabel2):
    
    pyplot.figure(1)
    pyplot.subplot(211)
    pyplot.plot(xarray1, yarray1)
    pyplot.ylabel(ylabel1)
    pyplot.xlabel(xlabel1)
    pyplot.subplot(212)
    pyplot.plot(xarray2, yarray2)
    pyplot.ylabel(ylabel2)
    pyplot.xlabel(xlabel2)
    pyplot.show()             
    
    return None


def createWavFile(data):

    wavfile = open("output.wav", 'w')    
    audioData = int16(data)
    wavfile.setparams((1,2,96000,len(audioData), 'NONE', 'not compressed'))
    wavfile.writeframes(audioData)
    wavfile.close()
    
    
    
    