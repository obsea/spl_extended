#!/usr/bin/python3
from NaxysHydrophone import NaxysHydrophone
import SPL
from numpy import copy, append
import threading
import argparse
import pandas as pd
import datetime
import os
from save_wav import save_to_wavfile
from time import strftime
from time import time as gettime

#Parameters
port = 15000 #UDP port where data is received
TGREEN =  '\033[32m' # Green Text
ENDC = '\033[m' # reset to the defaults
timer = 1
saveWAV = True
path='/home/enoc/test/testSPL'

csv = pd.DataFrame(columns=['Time','Global SPL','25 Hz','31.5 Hz','40 Hz','50 Hz','63 Hz','80 Hz','100 Hz','125 Hz','160 Hz',
                            '200 Hz','250 Hz','315 Hz','400 Hz','500 Hz','630 Hz','800 Hz','1000 Hz','1250 Hz','1600 Hz'
                            ,'2000 Hz','2500 Hz','3150 Hz','4000 Hz','5000 Hz','6300 Hz','8000 Hz','10000 Hz','12500 Hz'
                            ,'16000 Hz','20000 Hz', 'Remaining Hz'])

#Input arguments parse
ap = argparse.ArgumentParser()
ap.add_argument("-time", required=True, type=int, default = 5,
                help="Time to accumulate data. Default value is 5 seconds.")
ap.add_argument("-plot", type=bool, default = False,
                help="True/False, plot power spectrum. Default False.")
args = vars(ap.parse_args())
if args["time"] < 0:
    raise AssertionError("The -time command line argument should "
                         "be a positive integer")

#Data processing thread function
def processDataSPL(data, sensitivitydB, gaindB, k, samplingFreq,time):

    csvData = []
    global csv
    global timer
    ts = datetime.datetime.now().timestamp()
    readable = datetime.datetime.fromtimestamp(ts).isoformat()
    csvData = append(csvData,readable)
    SPLtime = SPL.timeBasedSPL(data, sensitivitydB, gaindB, k)
    SPLSpectrum = SPL.freqBasedSPL(data, sensitivitydB, gaindB, k)

    print(TGREEN + "[SPL] The computed SPL (based on time) over {} seconds of data is: {:04.2f} dB ref 1 uPa ".format(time,
                                                                                                       SPLtime) + ENDC)
    print(TGREEN + "[SPL] The computed SPL (based on spectrum) over {} seconds of data is: {:04.2f} dB ref 1 uPa ".format(time,
                                                                                                            SPLSpectrum) + ENDC)
    csvData = append(csvData,round(SPLtime,2))
    bandsPower = SPL.freqBandsSPL(data,
                                  sensitivitydB,
                                  gaindB,
                                  k,
                                  samplingFreq, args["plot"])
    pow = 0
    for i in range(len(bandsPower)):
        #print(
        #   TGREEN + "[SPL] Computed SPL of {} Hz third octave band (Low: {:04.2f} Hz High: {:04.2f} Hz) is: {:04.2f} dB ref 1 uPa".format(
        #      bandsPower[i][0][1], bandsPower[i][0][0], bandsPower[i][0][2], 10 * SPL.log10(bandsPower[i][1])) + ENDC)
        csvData = append(csvData, round(10 * SPL.log10(bandsPower[i][1]),2))
        pow = pow + bandsPower[i][1]
    #print( TGREEN + "[SPL] The total SPL (based on 25-20000 Hz third octaves)  is: {:04.2f} dB ref 1 uPa ".format(10 * SPL.log10(pow)) + ENDC)
    print(
        TGREEN + "[SPL] The computed SPL (based on third octave bands) over {} seconds of data is: {:04.2f} dB ref 1 uPa ".format(
            time,
            10*SPL.log10(pow)) + ENDC)
    auxCsv = pd.DataFrame([csvData[:]],columns=['Time','Global SPL','25 Hz','31.5 Hz','40 Hz','50 Hz','63 Hz','80 Hz','100 Hz','125 Hz','160 Hz',
                            '200 Hz','250 Hz','315 Hz','400 Hz','500 Hz','630 Hz','800 Hz','1000 Hz','1250 Hz','1600 Hz'
                            ,'2000 Hz','2500 Hz','3150 Hz','4000 Hz','5000 Hz','6300 Hz','8000 Hz','10000 Hz','12500 Hz'
                            ,'16000 Hz','20000 Hz', 'Remaining Hz'])

    csv = csv.append(auxCsv,ignore_index = True)
    print("Timer: ",timer)
    if timer == 60:
        print(TGREEN + "[SPL] Saving ", time, " seconds of data to csv file..." + ENDC)
        
        # 
        folder = path + '/' + strftime('%Y/%m/%d/%H')
        if not os.path.exists(folder): 
            os.makedirs(folder)        
    
        timestamp = gettime()
        filename = folder + '/' + str(timestamp) +'.csv'
        
        csv.to_csv(filename)
        csv=csv.iloc[0:0]
    timer += 1


print("-NAXYS HYDROPHONE ONLINE DATA ACQUISITION-")
hydrophone = NaxysHydrophone(port)
print("Initializing system...")
initialTime = datetime.datetime.now().timestamp()
while datetime.datetime.fromtimestamp(initialTime).second != 0 :
    initialTime = datetime.datetime.now().timestamp()

while True:
    data = hydrophone.accummulateData(args["time"])
    if data.size == 0:
        print("Data not reived correctly")
    else:
        threadData = copy(data)
        SPLThread = threading.Thread(target=processDataSPL,args=(threadData, hydrophone.sensitivitydB, hydrophone.gain, hydrophone.voltageConversion,  hydrophone.samplingFreq, args["time"]))
        SPLThread.start()
        if saveWAV:
            WAVData = copy(data)
            WAVThread = threading.Thread(target=save_to_wavfile, args=(WAVData, hydrophone.samplingFreq, path))
            WAVThread.start()
        if timer > 60:
            timer = timer - 60
